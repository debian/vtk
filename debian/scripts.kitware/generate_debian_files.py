#!/usr/bin/env python

import os.path
import sys

import vtk_debian_config
import vtk_debian_utils


GENRULES_MK_FORMAT = \
"""gen_extra_flags += \\
        {0}
"""


BINARY_PACKAGE_FORMAT = \
"""Package: {0}
Architecture: any
Depends:
        {1}
Description: {2}
 {2}

"""


if len(sys.argv) < 3:
    print("""Usage: %s <vtk binary dir|modules.json> <debian package dir>""" % sys.argv[0])
    exit(1)

vtk_binary_dir = sys.argv[1]
debian_package_dir = sys.argv[2]

vtk_modules = vtk_debian_utils.read_modules_from_file_or_directory(vtk_binary_dir)

extra_flags = " \\\n        ".join(vtk_modules.generate_cmake_args())

with open(os.path.join(debian_package_dir, "debian", "genrules.mk"), "w") as f:
    f.write(GENRULES_MK_FORMAT.format(extra_flags))

enabled_modules = vtk_modules.get_enabled_modules()
build_depends = set()
for module in enabled_modules:
    if module.is_third_party():
        build_depends.update(module.get_alias_dev_depends())
    else:
        build_depends.update(module.get_add_dev_depends())

with open(os.path.join(os.path.dirname(__file__), "control.in"), "r") as f:
    control_in = f.read()

with open(os.path.join(debian_package_dir, "debian", "control"), "w") as f:
    f.write(control_in.format(build_depends=",\n        ".join(sorted(build_depends))))
