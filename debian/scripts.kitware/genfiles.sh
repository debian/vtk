#!/bin/sh

set -e

TOPDIR=$(pwd)

rm -rf debian/fakebuild/
mkdir -p debian/fakebuild/
cd debian/fakebuild/
cmake "${TOPDIR}" -DVTK_GENERATE_MODULES_JSON=ON
cd "${TOPDIR}"

debian/scripts.kitware/generate_debian_files.py debian/fakebuild/ .
