import json
import os.path

import vtk_debian_config


class Module:
    def __init__(self, module_collection, name):
        self.name = name
        self.module_collection = module_collection
        self.is_test = False
        self.exclude_from_wrapping = False
        self.enabled = True
        self.depends = set()
        self.private_depends = set()
        self.implements = set()

    def depends_on_module(self, module):
        return self._depends_on_module(module, set())

    def _depends_on_module(self, module, cache):
        if self == module:
            return True

        if self in cache:
            return False
        cache.add(self)

        for m in self.depends:
            if m._depends_on_module(module, cache):
                return True

        return False

    def _get_config_value(self, name, default_value):
        try:
            this_module_config = self.module_collection.config[self.name]
        except KeyError:
            return default_value

        try:
            return this_module_config[name]
        except KeyError:
            return default_value

    def is_self_blacklisted(self):
        return self._get_config_value("blacklisted", False)

    def is_blacklisted(self):
        return self._is_blacklisted(set())

    def _is_blacklisted(self, cache):
        if self.is_self_blacklisted():
            return True

        if self in cache:
            return False
        cache.add(self)

        for module in self.depends:
            if module._is_blacklisted(cache):
                return True

        return False

    def is_third_party(self):
        return self._get_config_value("third_party", False)

    def get_library_name(self):
        return "libvtk9-{0}".format(self.name.lower())

    def get_dev_name(self):
        return "libvtk9-{0}-dev".format(self.name.lower())

    def get_add_depends(self):
        return self._get_config_value("add_depends", [])

    def get_add_dev_depends(self):
        return self._get_config_value("add_dev_depends", [])

    def get_alias_depends(self):
        alias_depends = [self.get_library_name()] + \
                self._get_config_value("add_alias_depends", [])
        return self._get_config_value("override_alias_depends", alias_depends)

    def get_alias_dev_depends(self):
        alias_dev_depends = [self.get_dev_name()] + \
                self._get_config_value("add_alias_dev_depends", [])
        return self._get_config_value("override_alias_dev_depends", alias_dev_depends)

    def get_depends(self):
        depends = []
        for m in self.depends:
            depends += m.get_alias_depends()
        depends += self.get_add_depends()
        return self._get_config_value("override_depends", depends)

    def get_dev_depends(self):
        dev_depends = self.get_alias_depends()
        for m in self.depends:
            dev_depends += m.get_alias_dev_depends()
        dev_depends += self.get_add_dev_depends()
        return self._get_config_value("override_dev_depends", dev_depends)


class ModuleGroup:
    def __init__(self, module_collection, name):
        self.name = name
        self.module_collection = module_collection
        self.modules = set()

    def is_blacklisted(self):
        for module in self.modules:
            if module.is_blacklisted():
                return True

        return False

    def contains_module(self, module):
        for m in self.modules:
            if m.depends_on_module(module):
                return True

        return False


class ModuleCollection:
    def __init__(self, config=vtk_debian_config.MODULE_CONFIG):
        self.modules = dict()
        self.groups = dict()
        self.config = config

    def get_enabled_modules(self):
        modules = set()

        for module_name in self.modules:
            module = self.modules[module_name]
            if not module.is_blacklisted() and not module.is_test:
                modules.add(module)

        return modules

    def get_enabled_groups_and_modules(self):
        groups = set()
        modules = set()

        for group_name in self.groups:
            group = self.groups[group_name]
            if not group.is_blacklisted():
                groups.add(group)

        for module_name in self.modules:
            module = self.modules[module_name]
            if not module.is_blacklisted() and not module.is_test:
                add = True
                for group in groups:
                    if group.contains_module(module):
                        add = False
                        break
                if add:
                    modules.add(module)

        return groups, modules

    def generate_cmake_args(self):
        args = list()

        enabled_groups, enabled_modules = self.get_enabled_groups_and_modules()

        for group_name in sorted(self.groups):
            group = self.groups[group_name]
            if group in enabled_groups:
                flag = "ON"
            else:
                flag = "OFF"
            args.append("-DVTK_Group_{0}={1}".format(group.name, flag))

        for module in sorted(enabled_modules, key=lambda m: m.name):
            args.append("-DModule_{0}=ON".format(module.name))

        return args


def read_modules_from_file(filename):
    modules = ModuleCollection()

    with open(filename, "r") as f:
        modules_json = json.load(f)

    for module_json in modules_json["modules"]:
        module_name = module_json["name"]
        module = Module(modules, module_name)

        module.is_test = module_json["is_test"]
        module.exclude_from_wrapping = module_json["exclude_from_wrapping"]
        module.enabled = module_json["enabled"]

        modules.modules[module_name] = module

    # Second pass to set up dependencies now that all modules have been created
    for module_json in modules_json["modules"]:
        module_name = module_json["name"]
        module = modules.modules[module_name]

        module.depends.update(modules.modules[n] for n in module_json["depends"])
        module.private_depends.update(modules.modules[n] for n in module_json["private_depends"])
        module.implements.update(modules.modules[n] for n in module_json["implements"])

    for group_json in modules_json["groups"]:
        group_name = group_json["name"]
        group = ModuleGroup(modules, group_name)

        group.modules.update(modules.modules[n] for n in group_json["modules"])

        modules.groups[group_name] = group

    return modules

def read_modules_from_file_or_directory(filename):
    try:
        vtk_modules = read_modules_from_file(os.path.join(filename, "modules.json"))
    except IOError as e:
        if e.errno == os.errno.ENOTDIR:
            vtk_modules = read_modules_from_file(filename)
        else:
            raise

    return vtk_modules
