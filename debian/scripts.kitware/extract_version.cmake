include("${CMAKE_CURRENT_LIST_DIR}/../../CMake/vtkVersion.cmake")

execute_process(COMMAND ${CMAKE_COMMAND} -E echo
  "${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}.${VTK_BUILD_VERSION}")
