MODULE_CONFIG = {
        # TODO Turn wrapping back on once it's ready for Debian
        "vtkPython": {
                "blacklisted": True,
        },
        "vtkWrappingJava": {
                "blacklisted": True,
        },
        "vtkWrappingTools": {
                "blacklisted": True,
        },

        "vtkDomainsMicroscopy": {
                "add_depends": ["libopenslide0"],
                "add_dev_depends": ["libopenslide-dev"],
        },
        "vtkDomainsParallelChemistry": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev"],
        },
        "vtkFiltersOpenTurns": {
                "add_depends": ["libopenturns0.10"],
                "add_dev_depends": ["libopenturns-dev"],
        },
        "vtkFiltersReebGraph": {
                "add_dev_depends": ["libboost-dev"],
        },
        "vtkGUISupportQt": {
                "add_depends": ["libqt5widgets5", "libqt5x11extras5"],
                "add_dev_depends": [
                        "libqt5x11extras5-dev",
                        "qtbase5-dev",
                        "qttools5-dev",
                ],
        },
        "vtkGUISupportQtOpenGL": {
                "add_depends": ["libqt5opengl5"],
                "add_dev_depends": ["libqt5opengl5-dev"],
        },
        "vtkIOADIOS": {
                "blacklisted": True,
        },
        "vtkIOFFMPEG": {
                "add_depends": [
                        "libavcodec57",
                        "libavformat57",
                        "libavutil55",
                        "libswscale4",
                ],
                "add_dev_depends": [
                        "libavcodec-dev",
                        "libavformat-dev",
                        "libavutil-dev",
                        "libswscale-dev",
                ],
        },
        "vtkIOGDAL": {
                "add_depends": ["libgdal20"],
                "add_dev_depends": ["libgdal-dev"],
        },
        "vtkIOLAS": {
                "add_depends": ["liblas3", "liblas-c3"],
                "add_dev_depends": ["liblas-dev", "liblas-c-dev"],
        },
        "vtkIOMPIImage": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev"],
        },
        "vtkIOMPIParallel": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev"],
        },
        "vtkIOPDAL": {
                "add_depends": ["libpdal-base5"],
                "add_dev_depends": ["libpdal-dev"],
        },
        "vtkIOParallelNetCDF": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev"],
        },
        "vtkIOParallelXdmf3": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev", "libboost-dev"],
        },
        "vtkIOXdmf3": {
                "add_dev_depends": ["libboost-dev"],
        },
        "vtkInfovisBoost": {
                "add_dev_depends": ["libboost-dev"],
        },
        "vtkInfovisBoostGraphAlgorithms": {
                "add_dev_depends": ["libboost-dev"],
        },
        "vtkParallelMPI": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev"],
        },
        "vtkParallelMPI4Py": {
                "add_depends": ["mpi-default-bin"],
                "add_dev_depends": ["mpi-default-bin", "mpi-default-dev"],
        },
        "vtkRenderingFreeTypeFontConfig": {
                "add_depends": ["libfontconfig"],
                "add_dev_depends": ["libfontconfig-dev"],
        },
        "vtkRenderingOSPRay": {
                "blacklisted": True,
        },
        "vtkRenderingOculus": {
                "blacklisted": True,
        },
        "vtkRenderingOpenGL2": {
                "add_depends": ["libxt6"],
                "add_dev_depends": ["libxt-dev"],
        },
        "vtkRenderingOpenVR": {
                "blacklisted": True,
        },
        "vtkRenderingOptiX": {
                "blacklisted": True,
        },
        "vtkVTKm": {
                "blacklisted": True,
        },
        "vtkdiy2": {
                "blacklisted": True,
                "third_party": True,
        },
        "vtkdoubleconversion": {
                "third_party": True,
                "override_alias_depends": ["libdouble-conversion1"],
                "override_alias_dev_depends": ["libdouble-conversion-dev"],
        },
        "vtkeigen": {
                "third_party": True,
                "override_alias_depends": [],
                "override_alias_dev_depends": ["libeigen3-dev"],
        },
        "vtkexpat": {
                "third_party": True,
                "override_alias_depends": ["libexpat1"],
                "override_alias_dev_depends": ["libexpat1-dev"],
        },
        "vtkfreetype": {
                "third_party": True,
                "override_alias_depends": ["libfreetype6"],
                "override_alias_dev_depends": ["libfreetype6-dev"],
        },
        "vtkgl2ps": {
                "third_party": True,
                "override_alias_depends": ["libgl2ps1.4 (>= 1.4.0+dfsg1-2)"],
                "override_alias_dev_depends": ["libgl2ps-dev (>= 1.4.0+dfsg1-2)"],
        },
        "vtkglew": {
                "third_party": True,
                "override_alias_depends": ["libglew2.0"],
                "override_alias_dev_depends": ["libglew-dev"],
        },
        "vtkhdf5": {
                "third_party": True,
                "override_alias_depends": ["libhdf5-100"],
                "override_alias_dev_depends": ["libhdf5-dev"],
        },
        "vtkjpeg": {
                "third_party": True,
                "override_alias_depends": ["libjpeg62-turbo"],
                "override_alias_dev_depends": ["libjpeg62-turbo-dev"],
        },
        "vtkjsoncpp": {
                "third_party": True,
                "override_alias_depends": ["libjsoncpp1"],
                "override_alias_dev_depends": ["libjsoncpp-dev"],
        },
        "vtkkissfft": {
                "blacklisted": True,
                "third_party": True,
        },
        "vtklibharu": {
                "blacklisted": True,
                "third_party": True,
        },
        "vtklibproj4": {
                "third_party": True,
                "override_alias_depends": ["libproj13"],
                "override_alias_dev_depends": ["libproj-dev"],
        },
        "vtklibxml2": {
                "third_party": True,
                "override_alias_depends": ["libxml2"],
                "override_alias_dev_depends": ["libxml2-dev"],
        },
        "vtklz4": {
                "third_party": True,
                "override_alias_depends": ["liblz4-1"],
                "override_alias_dev_depends": ["liblz4-dev"],
        },
        "vtklzma": {
                "third_party": True,
                "override_alias_depends": ["liblzma5"],
                "override_alias_dev_depends": ["liblzma-dev"],
        },
        "vtknetcdf": {
                "third_party": True,
                "override_alias_depends": ["libnetcdf13"],
                "override_alias_dev_depends": ["libnetcdf-dev"],
        },
        "vtkogg": {
                "third_party": True,
                "override_alias_depends": ["libogg0"],
                "override_alias_dev_depends": ["libogg-dev"],
        },
        "vtkpegtl": {
                "blacklisted": True,
                "third_party": True,
        },
        "vtkpng": {
                "third_party": True,
                "override_alias_depends": ["libpng16-16"],
                "override_alias_dev_depends": ["libpng-dev"],
        },
        "vtksqlite": {
                "third_party": True,
                "override_alias_depends": ["libsqlite3-0"],
                "override_alias_dev_depends": ["libsqlite3-dev"],
        },
        "vtktheora": {
                "third_party": True,
                "override_alias_depends": ["libtheora0"],
                "override_alias_dev_depends": ["libtheora-dev"],
        },
        "vtktiff": {
                "third_party": True,
                "override_alias_depends": ["libtiff5"],
                "override_alias_dev_depends": ["libtiff-dev"],
        },
        "vtkxdmf2": {
                "blacklisted": True,
                "third_party": True,
        },
        "vtkxdmf3": {
                "blacklisted": True,
                "third_party": True,
                "add_dev_depends": ["libboost-dev"],
        },
        "vtkzfp": {
                "blacklisted": True,
                "third_party": True,
        },
        "vtkzlib": {
                "third_party": True,
                "override_alias_depends": ["zlib1g"],
                "override_alias_dev_depends": ["zlib1g-dev"],
        },
}
