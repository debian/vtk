#!/usr/bin/env python

from __future__ import print_function

import os.path
import sys

import vtk_debian_config
import vtk_debian_utils


if len(sys.argv) < 2:
    print("""Usage: %s <vtk binary dir|modules.json>""" % sys.argv[0])
    exit(1)

vtk_binary_dir = sys.argv[1]

vtk_modules = vtk_debian_utils.read_modules_from_file_or_directory(vtk_binary_dir)
cmake_args = [ \
        "-DVTK_USE_SYSTEM_LIBRARIES=ON", \
        "-DVTK_USE_SYSTEM_NETCDFCPP=OFF", \
        "-DVTK_WRAP_JAVA=ON", \
        "-DVTK_WRAP_PYTHON=ON", \
] + vtk_modules.generate_cmake_args()
print(";".join(cmake_args), end="")
